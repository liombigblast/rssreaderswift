//
//  newsArticleDetail_ViewController.swift
//  rssReaderSwift
//
//  Created by Liom on 25/02/2016.
//  Copyright © 2016 Liom. All rights reserved.
//

import UIKit

class newsArticleDetail_ViewController: UIViewController {

    @IBOutlet weak var newsTitleLabel: UILabel!
    @IBOutlet weak var newsImage: UIImageView!
    @IBOutlet weak var newsDateLabel: UILabel!
    @IBOutlet weak var contentWebView: UIWebView!
    
    var titleString = ""
    var dateString = ""
    var imageUrlString = ""
    var descriptionString = ""
    var offline:Bool = true
    var imageData: NSData = NSData()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        //On affiche les données
        newsTitleLabel.text = self.titleString
        newsDateLabel.text = self.dateString
        
        //Si on est online
        if(self.offline == false){
            //On récupère l'image depuis l'url
            let url = NSURL(string: self.imageUrlString)
            let data = NSData(contentsOfURL: url!)
            
            //Vérification si la connexion n'est pas perdue
            if(data != nil){
                self.newsImage.image = UIImage(data: data!)
            }
        }else{
            if(self.imageData != NSData()){
                self.newsImage.image = UIImage(data: self.imageData)
            }
        }
        
        //On charge la vue web avec le contenu de l'article
        self.contentWebView.loadHTMLString(descriptionString, baseURL: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
