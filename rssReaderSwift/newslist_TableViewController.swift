//
//  newslist_TableViewController.swift
//  rssReaderSwift
//
//  Created by Liom on 25/02/2016.
//  Copyright © 2016 Liom. All rights reserved.
//

import UIKit

class newslist_TableViewController: UITableViewController,NSXMLParserDelegate {
    
    var urlString = NSURL(string: "http://www.lemonde.fr/rss/une.xml")
    var xmlParser: NSXMLParser!
    var entryTitle: String!
    var entryDescription: String!
    var entryLink: String!
    var entryImage: String!
    var entryDate: String!
    var currentParsedElement:String! = String()
    var entryDictionary: [String:String]! = Dictionary()
    var entriesArray:[Dictionary<String, String>]! = Array()
    var imagesDictionary: [String:NSData] = [:]
    
    var refreshController = UIRefreshControl()
    let offlineVariable:NSUserDefaults = NSUserDefaults.standardUserDefaults()
    var offline:Bool = false
    
    @IBOutlet weak var articlesFeed_TableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //Chargement du flux
        self.parseFeed()
        
        //On recharge les données stockées
        self.reloadSavedData()
        
        //Ajout du refreshCrontoller
        self.refreshControl = self.refreshController
        
        //On définit l'action du refresher
        self.refreshController.addTarget(self, action: "parseFeed", forControlEvents: UIControlEvents.ValueChanged)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.entriesArray.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("articleCell", forIndexPath: indexPath) as! newsarticle_TableViewCell

        //Configuration de la cellule
        cell.articleTitle.text = self.entriesArray[indexPath.row]["title"]
        
        //On récupère l'image si l'adresse est renseignée
        if(self.entriesArray[indexPath.row]["enclosure"] != nil && self.offline == false){
            //On récupère l'image à l'URL indiquée
            let url = NSURL(string: self.entriesArray[indexPath.row]["enclosure"]!)
            let data = NSData(contentsOfURL: url!)
            if(data != nil){
                cell.articleImage.image = UIImage(data: data!)
                //On sauvegarde l'image récupérée
                self.imagesDictionary[String(indexPath.row)] = data!
            }

            if(self.imagesDictionary.count > 0){
                self.offlineVariable.setObject(self.imagesDictionary, forKey: "offlineImages")
            }
        }
        
        //Si on est offline, on récupère les images sauvegardées
        if(self.offline == true && self.imagesDictionary.count > 0){
            cell.articleImage.image = UIImage(data: self.imagesDictionary[String(indexPath.row)]!)
        }
        
        return cell
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "listToDetails"){
            
            //On transmet les informations de l'article à notre vue de détail
            let details = segue.destinationViewController as!  newsArticleDetail_ViewController
            let indexPath = tableView.indexPathForCell(sender as! newsarticle_TableViewCell)
            
            details.titleString = self.entriesArray[indexPath!.row]["title"]!
            details.imageUrlString = self.entriesArray[indexPath!.row]["enclosure"]!
            details.dateString = self.entriesArray[indexPath!.row]["pubDate"]!
            details.descriptionString = self.entriesArray[indexPath!.row]["description"]!
            details.offline = self.offline
            
            //Si on est offline
            if(self.offline == true){
                //On  transmet l'image en donnée
                details.imageData = self.imagesDictionary[String(indexPath!.row)]!
            }
            
        }
    }
    
    /*
    *       FONCTIONS SPECIFIQUES
    */
    
    //La fonction va parser le flux RSS demandé
    func parseFeed(){
        
        let rssUrlRequest:NSURLRequest = NSURLRequest(URL:urlString!)
        let queue:NSOperationQueue = NSOperationQueue()
        
        NSURLConnection.sendAsynchronousRequest(rssUrlRequest, queue: queue) {
            (response, data, error) -> Void in
            
            if((error) != nil || data == NSObject()){
                self.offline = true
                //On débloque le refresher
                self.refreshController.endRefreshing()
                self.reloadSavedData()
            }else{
                self.xmlParser = NSXMLParser(data: data!)
                self.xmlParser.delegate = self
                self.xmlParser.parse()
            }
        }

    }
    
    //Cette fonction est appelée à chaque début d'élément du flux pour récupérer les balises souhaitées
    func parser(parser: NSXMLParser!,
        didStartElement elementName: String!,
        namespaceURI: String!,
        qualifiedName: String!,
        attributes attributeDict: [String : String]!){
            if elementName == "title"{
                entryTitle = String()
                currentParsedElement = "title"
            }
            if elementName == "description"{
                entryDescription = String()
                currentParsedElement = "description"
            }
            if elementName == "link"{
                entryLink = String()
                currentParsedElement = "link"
            }
            
            //On s'occupe due la date
            if elementName == "pubDate"{
                entryDate = String()
                currentParsedElement = "pubDate"
            }
            
            if elementName == "enclosure"{
                //L'url de l'image est stockée dans les attributs
                entryImage = attributeDict ["url"]
                currentParsedElement = "enclosure"
            }
    }

    //Cette fonction va stocker les valeurs trouvées dans les balises de l'élément courant
    func parser(parser: NSXMLParser!,
        foundCharacters string: String!){
            
            //On s'occupe du titre
            if currentParsedElement == "title"{
                entryTitle = entryTitle + string
            }
            
            //On s'occupe de la description
            if currentParsedElement == "description"{
                entryDescription = entryDescription + string
            }
            
            //On s'occupe du lien
            if currentParsedElement == "link"{
                entryLink = entryLink + string
            }
            
            //On s'occupe du lien
            if currentParsedElement == "pubDate"{
                entryDate = entryDate + string
            }
    }
    
    
    //Cette fonction est appelée à chaque fin d'élément pour le stocker dans notre tableau d'articles
    func parser(parser: NSXMLParser!,
        didEndElement elementName: String!,
        namespaceURI: String!,
        qualifiedName qName: String!){
            if elementName == "title"{
                entryDictionary["title"] = entryTitle
            }
            if elementName == "link"{
                entryDictionary["link"] = entryLink
            }
            if elementName == "enclosure"{
                entryDictionary["enclosure"] = entryImage
            }
            if elementName == "description"{
                entryDictionary["description"] = entryDescription
            }
            if elementName == "pubDate"{
                entryDictionary["pubDate"] = entryDate
                //On n'ajoute pas le premier élément qui est un entête
                if(entryImage != nil){
                    self.entriesArray.append(entryDictionary)
                }
            }
    }
    
    //Cette fonction est appelée à la fin du parsing afin de rafraichir et afficher notre table view
    func parserDidEndDocument(parser: NSXMLParser!){
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            self.articlesFeed_TableView.reloadData()
        })
        
        //On prévient le refersher que l'action est finie
        self.refreshController.endRefreshing()
        
        //Si le reader retourne des résultats
        if(self.entriesArray.count > 0){
            //On efface les données sauvegardées
            NSUserDefaults.standardUserDefaults().removeObjectForKey("offlineFeed")
            
            //on sauvegarde le resultat dans une variable sauvegardées
            self.offlineVariable.setObject(self.entriesArray, forKey: "offlineFeed")
            //On flag comme étant online
            self.offline = false
            
        }
    }
    
    //Fonction permettant le rechargement des données sauvegardées
    func reloadSavedData(){
        //On recharge les données stockées
        if(self.offline == true){
            if(self.offlineVariable.valueForKey("offlineFeed")) != nil{
                self.entriesArray =  self.offlineVariable.valueForKey("offlineFeed") as! [Dictionary<String, String>]
                self.articlesFeed_TableView.reloadData()
            }
            if(self.offlineVariable.valueForKey("offlineImages")) != nil{
                self.imagesDictionary = self.offlineVariable.valueForKey("offlineImages") as! Dictionary
                self.articlesFeed_TableView.reloadData()
            }
        }
    }

}
