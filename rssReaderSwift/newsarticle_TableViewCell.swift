//
//  newsarticle_TableViewCell.swift
//  rssReaderSwift
//
//  Created by Liom on 25/02/2016.
//  Copyright © 2016 Liom. All rights reserved.
//

import UIKit

class newsarticle_TableViewCell: UITableViewCell {
    
    @IBOutlet weak var articleTitle: UILabel!
    @IBOutlet weak var articleImage: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
